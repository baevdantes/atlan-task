# atlan-task

## Main Details

1. This is the app for receiving data via queries. 
   It includes the following components:
   - Query field (textarea)
   - Submit button
   - Clear button (clear the query field and the selector)
   - Select input with prepared three queries (Get Posts, Get Users, Get Comments)
   - The box with response where there is a list of items
   - Error message for cases when the error is received from backend

   I used [Mockend](https://mockend.com/) for receiving mock data.
2. The app was developed by Vue.js framework (Vue3 + Vite). 
   Also, I added few packages:
   - **Axios** for http request to the Mockend.
   - **VueVirtualScroller** for virtual scrolling of list with items (100 rows as example).
   - **VueRouter** for implementing routing to the app. Actually in that case there was no need to add routing generally
     but I've added it for *Not Found / 404* page.
   - **[Histoire](https://histoire.dev/)** for developing and testing components (stories).
   - **TailwindCSS** for developing layouts. This is a very useful library which allows to add any styles just using Tailwind classes.

3. The average page load time of the application is 0.5s. I've measured the time by Lighthouse in Chrome. Also I've checked the rest of metrics:
   
   - Performance - 100
   - Accessibility - 100
   - Best practices - 100
4. The optimisations I did to decrease the load time or increase performance:
   
   - I've used **lazy loading routes**. Vue Router fetches the route when entering the page for the first time.
   - **Virtual scroll** package for the large list of items.


# Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
