export const sizeTypes = {
  small: "small",
  large: "large",
  medium: "medium",
};
export const variantTypes = {
  outlined: "outlined",
  primary: "primary",
};
