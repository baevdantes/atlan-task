import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      component: () => import("../layouts/BaseLayout.vue"),
      children: [
        {
          path: "/",
          name: "home",
          component: () => import("../views/HomeView.vue"),
        },
        {
          path: "/about",
          name: "about",
          component: () => import("../views/AboutView.vue"),
        },
      ],
    },
    {
      path: "/",
      component: () => import("../layouts/EmptyLayout.vue"),
      children: [
        {
          name: "not-found",
          path: "/:catchAll(.*)*",
          component: () => import("../views/NotFoundView.vue"),
        },
      ],
    },
  ],
});

export default router;
